import firebase from "firebase";
import { admin, db } from "../utils/admin";
import { Request, Response } from "express";
import { User } from "../models/User";
import { validateSignupData } from "../utils/validators";
import { firebaseConfig } from "../firebase/config";
// import busboy from "busboy";
// import path from "path";
// import os from "os";
// import fs from "fs";
// import { v4 as uuidv4 } from "uuid";

const USERS_COLLECTION = "users";

firebase.initializeApp(firebaseConfig);
firebase.auth().setPersistence(firebase.auth.Auth.Persistence.NONE);

export const signup = async (request: Request, response: Response) => {
  const newUser: User = {
    email: request.body.email,
    name: request.body.name,
    password: request.body.password,
  };
  const expiresIn = 60 * 60 * 24 * 5 * 1000;
  const { isValid, errors } = validateSignupData(newUser);
  if (!isValid) return response.status(400).send(errors);

  try {
    const createNewUser = await firebase
      .auth()
      .createUserWithEmailAndPassword(newUser.email, newUser.password);

    const newUserRef = db.doc(`${USERS_COLLECTION}/${createNewUser.user?.uid}`);

    await newUserRef.set({
      name: newUser.name,
      email: newUser.email,
    });

    const idToken = await createNewUser.user?.getIdToken();

    const sessionCookie = await admin
      .auth()
      .createSessionCookie(idToken!, { expiresIn });
    const options = { maxAge: expiresIn, httpOnly: true, secure: true };
    response.cookie("session", sessionCookie, options);
    return response.send(idToken);
  } catch (error) {
    var errorCode = error.code;
    var errorMessage = error.message;
    if (errorCode == "auth/weak-password") {
      response.status(401).send("The password is too weak.");
    } else {
      console.error(error);
      response.status(401).send(errorMessage);
    }
  }
};

// export const signup = (
//   request: Request,
//   response: Response
// ) => {
//   const newUser: User = {
//     name: request.body.name,
//     email: request.body.email,
//     password: request.body.password,
//     confirmPassword: request.body.confirmPassword,
//   };

//   const defaultImg = "no-img.png"; //TODO: This should be a const... it is the default image that is added when a user signs up

//   let token: string | undefined, userId: string | undefined;

//   const newUserRef = db.doc(`${USERS_COLLECTION}/${newUser.name}`);
//   newUserRef
//     .get()
//     .then((documentSnapshot) => {
//       if (documentSnapshot.exists) {
//         return Promise.reject(new Error(`User ${newUser.name} already exists`));
//       } else {
//         return firebase.default
//           .auth()
//           .createUserWithEmailAndPassword(newUser.email, newUser.password);
//       }
//     })
//     .then((data) => {
//       userId = data?.user?.uid;
//       return data?.user?.getIdToken();
//     })
//     .then((idToken) => {
//       token = idToken;
//       const user: UserCredentials = {
//         name: newUser.name,
//         email: newUser.email,
//         createdAt: new Date().toISOString(),
//         imageUrl: `https://firebasestorage.googleapis.com/v0/b/${firebaseConfig.storageBucket}/o/${defaultImg}?alt=media`, //TODO: Make this url a function, pass in image name only - pulls firebaseconfig from other file
//         userId,
//       };

//       return newUserRef.set(user);
//     })
//     .then(() => {
//       response.status(201).send({ token });
//     })
//     .catch((err) => {
//       console.error(err);
//       return response.status(500).send({ message: err.message });
//     });
// };

export const login = async (request: Request, response: Response) => {
  // const user: LoginUser = {
  //   email: request.body.email,
  //   password: request.body.password,
  // };
  // const { isValid, errors } = validateLoginData(user);
  // if (!isValid) return response.status(400).send(errors);

  try {
    const loginUser = await firebase
      .auth()
      .signInWithEmailAndPassword(request.body.email, request.body.password);
    const token = await loginUser.user?.getIdToken();
    return response.send(token);
  } catch (error) {
    console.error(error);
    return response.status(401).send(error);
  }
};

// export class ImageToBeUploaded {
//   filePath: string;
//   mimetype: string;
// }

// export const uploadImage = (
//   request: IGetUserAuthInfoRequest,
//   response: Response
// ) => {
//   let imageFileName: string;
//   let imageToBeUploaded = {} as ImageToBeUploaded;
//   const bboy: busboy.Busboy = new busboy({ headers: request.headers });

//   bboy.on(
//     "file",
//     (
//       _fieldname: string,
//       file: NodeJS.ReadableStream,
//       fileName: string,
//       _encoding: string,
//       mimetype: string
//     ) => {
//       const imageExtension = fileName.split(".")[
//         fileName.split(".").length - 1
//       ];
//       const imageFileName = `${uuidv4()}.${imageExtension}`;
//       const filePath = path.join(os.tmpdir(), imageFileName);
//       imageToBeUploaded = { filePath, mimetype };
//       file.pipe(fs.createWriteStream(filePath));
//     }
//   );

//   bboy.on("finish", () => {
//     admin
//       .storage()
//       .bucket()
//       .upload(imageToBeUploaded.filePath, {
//         resumable: false,
//         metadata: { contentType: imageToBeUploaded.mimetype },
//       })
//       .then(() => {
//         //TODO: this should be a const somewhere
//         const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${firebaseConfig.storageBucket}/o/${imageFileName}?alt=media`;
//         return db
//           .doc(`${USERS_COLLECTION}/${request.user.name}`)
//           .update({ imageUrl });
//       })
//       .then(() => {
//         return response.send({ message: "Image uploaded successfully" });
//       })
//       .catch((err) => {
//         console.error(err);
//         return response.status(500).send({ error: err.code });
//       });
//   });
// };
