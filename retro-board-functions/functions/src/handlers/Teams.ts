import { db } from "../utils/admin";
import { Team } from "../models/Team";
import { Request, Response } from "express";

const TEAMS_COLLECTION = "teams";

export const getTeams = async (_request: Request, response: Response) => {
  let teams: Team[] = [];
  const teamsSnapshots = await db.collection(TEAMS_COLLECTION).get();

  if (teamsSnapshots.empty) {
    return response.send([]);
  } else {
    teamsSnapshots.forEach((teamDoc) => {
      teams.push({
        id: teamDoc.id,
        name: teamDoc.data().name,
        code: teamDoc.data().code,
        colour: teamDoc.data().colour,
      });
    });
    return response.send(teams);
  }
};

export const getTeam = async (request: Request, response: Response) => {
  let team = {} as Team;
  const teamRef = db.doc(`${TEAMS_COLLECTION}/${request.params.teamId}`);
  try {
    const doc = await teamRef.get();

    if (!doc.exists) {
      return response.status(404).send({ error: "No team found" });
    } else {
      team = {
        id: doc.id,
        name: doc.data()?.name,
        code: doc.data()?.name,
        colour: doc.data()?.colour,
      };
    }

    response.send(team);
  } catch (error) {
    console.error(error);
    return response.send(error.message);
  }
};

export const addTeam = (request: Request, response: Response) => {
  const newTeam: Team = {
    name: request.body.name,
    code: request.body.code,
    colour: request.body.colour,
  };

  // TODO: Add validation if team already exists
  db.collection(TEAMS_COLLECTION)
    .add(newTeam)
    .then(() => {
      response.send({
        message: `Team ${newTeam.code}(${newTeam.name}) created successfully`,
      });
    })
    .catch((err) => {
      response.status(500).send({ error: `Could not add team due to: ${err}` });
    });
};
