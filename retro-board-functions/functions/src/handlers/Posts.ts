import { admin, db } from "../utils/admin";
import { NewPost, Post } from "../models/Post";
import { Request, Response } from "express";

export const POSTS_COLLECTION = "posts";

export const getPostsForSprint = async (
  request: Request,
  response: Response
) => {
  let posts: Post[] = [];
  const postsRef = db.collection(POSTS_COLLECTION);
  try {
    const postsSnapshots = await postsRef
      .where("sprintId", "==", request.params.sprintId)
      .get();

    if (postsSnapshots.empty) {
      return response.send([]);
    } else {
      postsSnapshots.forEach((postDoc) => {
        posts.push({
          id: postDoc.id,
          content: postDoc.data().content,
          likes: postDoc.data().likes,
          sprintId: postDoc.data().sprintId,
          type: postDoc.data().type,
          userId: postDoc.data().userId,
        });
      });
      return response.send(posts);
    }
  } catch (error) {
    console.error(error);
    return response.send(error.message);
  }
};

export const addPost = async (request: Request, response: Response) => {
  const newPost: NewPost = {
    content: request.body.content,
    sprintId: request.body.sprintId,
    type: request.body.type,
    likes: [],
    userId: request.body.userId || "Unknown user",
  };

  const postsRef = db.collection(POSTS_COLLECTION);

  try {
    const addedPostRef = await postsRef.add(newPost);
    const addedPost: Post = {
      ...newPost,
      id: addedPostRef.id,
    };
    return response.send(addedPost);
  } catch (error) {
    console.error(error);
    return response.send(error.message);
  }
};

export const likePost = async (request: Request, response: Response) => {
  const postRef = db.doc(`${POSTS_COLLECTION}/${request.params.postId}`);
  const USER_ID = request.body.userId;

  try {
    await postRef.update({
      likes: admin.firestore.FieldValue.arrayUnion(request.body.userId),
    });

    return response.send(USER_ID);
  } catch (error) {
    console.error(error);
    return response.send(error.message);
  }
};

export const unlikePost = async (request: Request, response: Response) => {
  const postRef = db.doc(`${POSTS_COLLECTION}/${request.params.postId}`);
  const USER_ID = request.body.userId;

  try {
    await postRef.update({
      likes: admin.firestore.FieldValue.arrayRemove(request.body.userId),
    });

    return response.send(USER_ID);
  } catch (error) {
    console.error(error);
    return response.send(error.message);
  }
};

export const deletePost = async (request: Request, response: Response) => {
  const postRef = db.doc(`${POSTS_COLLECTION}/${request.params.postId}`);

  try {
    await postRef.delete();

    return response.send(request.params.postId);
  } catch (error) {
    console.error(error);
    return response.send(error.message);
  }
};
