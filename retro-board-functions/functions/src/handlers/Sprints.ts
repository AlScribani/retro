import { db } from "../utils/admin";
import { Sprint, NewSprint, ActiveSprint } from "../models/Sprint";
import { Request, Response } from "express";
import { POSTS_COLLECTION } from "./Posts";

export const SPRINTS_COLLECTION = "sprints";

export const getSprintsByTeam = async (
  request: Request,
  response: Response
) => {
  let sprints: ActiveSprint[] = [];
  const sprintsRef = db.collection(SPRINTS_COLLECTION);
  try {
    const sprintSnapshots = await sprintsRef
      .orderBy("name")
      .where("teamId", "==", request.params.teamId)
      .get();

    if (sprintSnapshots.empty) {
      return response.send([]);
    } else {
      sprintSnapshots.forEach((sprintDoc) => {
        sprints.push({
          id: sprintDoc.id,
          name: sprintDoc.data().name,
          isComplete: sprintDoc.data().isComplete,
          teamId: sprintDoc.data().teamId,
          createdBy: sprintDoc.data().createdBy,
          lastUpdatedAt: sprintDoc.updateTime.toDate(),
        });
      });
      return response.send(sprints);
    }
  } catch (error) {
    console.error(error);
    return response.send(error.message);
  }
};

export const getSprint = async (request: Request, response: Response) => {
  let sprint = {} as Sprint;
  const sprintRef = db.doc(`${SPRINTS_COLLECTION}/${request.params.sprintId}`);

  try {
    const doc = await sprintRef.get();

    if (!doc.exists) {
      return response.status(404).send({ error: "No sprint found" });
    } else {
      sprint = {
        id: doc.id,
        name: doc.data()?.name,
        createdBy: doc.data()?.createdBy,
        isComplete: doc.data()?.isComplete,
        teamId: doc.data()?.teamId,
      };
    }
    response.send(sprint);
  } catch (error) {
    console.error(error);
    return response.send(error.message);
  }
};

export const createSprint = async (request: Request, response: Response) => {
  const sprintsRef = db.collection(SPRINTS_COLLECTION);

  const sprintsSnapshots = await sprintsRef
    .where("teamId", "==", request.body.teamId)
    .get();

  const sprintsCount = sprintsSnapshots ? sprintsSnapshots.size : 0;
  console.log("sprintsSnapshots", sprintsSnapshots);
  const newSprint: NewSprint = {
    name: `Sprint ${sprintsCount + 1}`,
    isComplete: false,
    teamId: request.body.teamId,
    createdBy: request.body.userId || "Unknown user",
  };

  try {
    const addedSprint = await sprintsRef.add(newSprint);
    return response.send(addedSprint.id);
  } catch (error) {
    console.error(error);
    return response.send(error.message);
  }
};

export const completeSprint = async (request: Request, response: Response) => {
  const sprintRef = db.doc(`${SPRINTS_COLLECTION}/${request.body.sprintId}`);

  try {
    const writeResult = await sprintRef.update({
      isComplete: true,
    });

    return response.send(writeResult);
  } catch (error) {
    console.error(error);
    return response.send(error.message);
  }
};

export const reactivateSprint = async (
  request: Request,
  response: Response
) => {
  const sprintRef = db.doc(`${SPRINTS_COLLECTION}/${request.body.sprintId}`);

  try {
    const writeResult = await sprintRef.update({
      isComplete: false,
    });

    return response.send(writeResult);
  } catch (error) {
    console.error(error);
    return response.send(error.message);
  }
};

export const deleteSprint = async (request: Request, response: Response) => {
  //TODO: Add comments when added
  console.log("request.params.sprintId", request.params.sprintId);
  const batch = db.batch();
  const sprintRef = db.doc(`${SPRINTS_COLLECTION}/${request.params.sprintId}`);
  const postsSnapshots = await db
    .collection(POSTS_COLLECTION)
    .where("sprintId", "==", request.params.sprintId)
    .get();
  try {
    await db.runTransaction(async (t) => {
      postsSnapshots.forEach((doc) => {
        batch.delete(doc.ref);
      });
      batch.commit();
      t.delete(sprintRef);
    });
    await sprintRef.delete();

    return response.send(request.params.sprintId);
  } catch (error) {
    console.error(error);
    return response.send(error.message);
  }
};
