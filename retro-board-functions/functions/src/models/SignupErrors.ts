export class SignupErrors {
  email: string;
  password: string;
  confirmPassword: string;
  name: string;
}
