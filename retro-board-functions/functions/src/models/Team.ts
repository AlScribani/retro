export class Team {
  id?: string;
  name: string;
  code: string;
  colour: string;
}
