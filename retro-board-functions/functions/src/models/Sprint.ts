import { DbUser } from "../models/User";

export class Sprint {
  id: string;
  name: string;
  isComplete: boolean;
  teamId: string;
  createdBy: string;
}

export type ActiveSprint = {
  id: string;
  name: string;
  teamId: string;
  isComplete: boolean;
  createdBy: string;
  lastUpdatedAt: Date;
  postCount?: number;
  likesCount?: number;
  actionsCount?: number;
  participants?: DbUser[];
};

export class NewSprint {
  name: string;
  isComplete: boolean;
  teamId: string;
  createdBy: string;
}
