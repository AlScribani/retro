export type Post = {
  id: string;
  content: string;
  sprintId: string;
  userId: string;
  likes: string[];
  type: string;
};

export type NewPost = {
  content: string;
  sprintId: string;
  userId: string;
  likes: string[];
  type: string;
};
