import express from "express";
import { UserCredentials } from "./User";

export interface IGetUserAuthInfoRequest extends express.Request {
  user: UserCredentials;
}
