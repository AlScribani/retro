export class User {
  name: string;
  email: string;
  password: string;
}

export class LoginUser {
  email: string;
  password: string;
}

export class DbUser extends User {
  id: string;
}

export class UserCredentials {
  name: string;
  email: string;
  createdAt: string;
  userId: string | undefined;
  imageUrl: string;
}
