import { LoginUser, User } from "../models/User";
import { SignupErrors } from "../models/SignupErrors";

const isEmpty = (input: string) => {
  if (input == undefined || input.trim() === "") {
    return true;
  }

  return false;
}; //TODO: This could be cleaned up a bit

const isEmail = (email: string) => {
  if (email !== undefined) {
    const regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return email.match(regEx) ? true : false;
  }
  
  return false;
}; //TODO: This could be cleaned up a bit

export const validateLoginData = (user: LoginUser) => {
  let errors = {} as SignupErrors;

  if (isEmpty(user.email)) {
    errors.email = "Email must not be empty";
  } else if (!isEmail(user.email)) {
    errors.email = "Must be a valid email address";
  }

  if (isEmpty(user.password)) {
    errors.password = "Must not be empty";
  }

  return {
    errors,
    isValid: Object.keys(errors).length === 0 ? true : false,
  };
};

export const validateSignupData = (user: User) => {
  let errors = {} as SignupErrors;

  if (isEmpty(user.name)) {
    errors.name = "Must not be empty";
  }

  if (isEmpty(user.email)) {
    errors.email = "Email must not be empty";
  } else if (!isEmail(user.email)) {
    errors.email = "Must be a valid email address";
  }

  if (isEmpty(user.password)) {
    errors.password = "Must not be empty";
  }

  return {
    errors,
    isValid: Object.keys(errors).length === 0 ? true : false,
  };
};
