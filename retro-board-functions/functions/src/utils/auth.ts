import { Request, Response } from "express";
import { admin } from "./admin";

export async function isAuthenticated(
  request: Request,
  response: Response,
  next: Function
) {
  const { authorization } = request.headers;

  if (!authorization) return handleUnauthorisedError(response);

  if (!authorization.startsWith("Bearer"))
    return handleUnauthorisedError(response);

  const split = authorization.split("Bearer ");
  if (split.length !== 2) return handleUnauthorisedError(response);

  const token = split[1];

  try {
    const decodedToken: admin.auth.DecodedIdToken = await admin
      .auth()
      .verifyIdToken(token);
    response.locals = {
      ...response.locals,
      uid: decodedToken.uid,
      
      role: decodedToken.role,
      email: decodedToken.email,
    };

    return next();
  } catch (err) {
    console.error(`${err.code} -  ${err.message}`);
    return handleUnauthorisedError(response);
  }
}

const handleUnauthorisedError = (response: Response) => {
  return response.status(401).send({ message: "Unauthorized" });
};

// // export const FBAuth = (
// //   request: express.Request,
// //   response: express.Response,
// //   next: express.NextFunction
// // ) => {
// //   let idToken;
// //   if (
// //     request.headers.authorization &&
// //     request.headers.authorization.startsWith("Bearer ")
// //   ) {
// //     idToken = request.headers.authorization.split("Bearer ")[1];
// //   } else {
// //     console.error("No auth token found");
// //     return response.status(403).json({ error: "Unauthorised" });
// //   }

// //   let userAuthInfoRequest: IGetUserAuthInfoRequest = <IGetUserAuthInfoRequest>(
// //     request
// //   );
// //   userAuthInfoRequest.user = {
// //     createdAt: "",
// //     email: "",
// //     name: "",
// //     userId: "",
// //     imageUrl: ""
// //   };

// //   admin
// //     .auth()
// //     .verifyIdToken(idToken)
// //     .then((decodedToken: admin.auth.DecodedIdToken) => {
// //       userAuthInfoRequest.user.userId = decodedToken.uid;
// //       return db
// //         .collection(USERS_COLLECTION)
// //         .where("userId", "==", userAuthInfoRequest.user.userId)
// //         .limit(1)
// //         .get();
// //     })
// //     .then((data) => {
// //       userAuthInfoRequest.user.name = data.docs[0].data().name;
// //       return next();
// //     })
// //     .catch((err) => {
// //       console.error("Error while verifying token ", err);
// //       return response.status(403).json(err);
// //     });
// // };
