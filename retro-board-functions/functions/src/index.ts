import * as functions from "firebase-functions";
import express from "express";
import cors from "cors";
// import { isAuthenticated } from "./utils/auth";
import { addTeam, getTeam, getTeams } from "./handlers/Teams";
import { signup, login } from "./handlers/Users";
import {
  getSprintsByTeam,
  getSprint,
  createSprint,
  completeSprint,
  reactivateSprint,
  deleteSprint,
} from "./handlers/Sprints";
import {
  addPost,
  deletePost,
  getPostsForSprint,
  likePost,
  unlikePost,
} from "./handlers/Posts";

const app = express();

app.use(cors());

const TEAM_API = "/team";
const USER_API = "/user";
const SPRINT_API = "/sprint";
const POST_API = "/post";

// Teams
app.get(TEAM_API, getTeams);
app.get(`${TEAM_API}/:teamId`, getTeam);
app.post(TEAM_API, addTeam);

// app.get(TEAMS_API, FBAuth, getTeams);
// app.post(TEAMS_API, FBAuth, addTeam);

// Sprints
app.get(`${SPRINT_API}/:teamId`, getSprintsByTeam);
app.get(`${SPRINT_API}/:teamId/:sprintId`, getSprint); //TODO: SprintId should change to sprint name... match on teamId & sprint name
app.post(`${SPRINT_API}`, createSprint);
app.post(`${SPRINT_API}/complete`, completeSprint);
app.post(`${SPRINT_API}/reactivate`, reactivateSprint);
app.delete(`${SPRINT_API}/:sprintId`, deleteSprint);

// Posts
app.get(`${POST_API}/:sprintId`, getPostsForSprint);
app.post(`${POST_API}`, addPost);
app.post(`${POST_API}/like/:postId`, likePost);
app.post(`${POST_API}/unlike/:postId`, unlikePost);
app.delete(`${POST_API}/:postId`, deletePost);

// Users
app.post(`${USER_API}/signup`, signup);
app.post(`${USER_API}/login`, login);
// app.post(`${USERS_API}/image`, FBAuth, uploadImage); //TODO: fix this endpoint up

export const api = functions.https.onRequest(app); // functions.region(firebaseRegion).https.onRequest(app);
