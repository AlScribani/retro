import React, { useContext } from "react";
import UserContext from "../root/state/auth/UserContext";

const HomeComponent = () => {
  const { user } = useContext(UserContext);
  return <h1>Welcome {user}</h1>;
};

export default HomeComponent;
