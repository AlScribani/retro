import React from "react";
import styled from "styled-components";
import CardBase from "react-bootstrap/Card";
import Paper from "@material-ui/core/Paper";

interface TeamCardProps {
  title: string;
  colour: string;
}

const TeamCard = ({ title, colour }: TeamCardProps) => {
  return (
    <CardContainer className="team-card-container">
      <Title>{title}</Title>
    </CardContainer>
  );
};

export default TeamCard;

const colours = ["#23F0C7", "#EF767A", "#7D7ABC", "#6457A6", "#FFE347"];

const handleColorType = (color: string) => {
  switch (color.toUpperCase()) {
    case "RED":
      return "#EF767A";
    case "GREEN":
      return "#23F0C7";
    case "YELLOW":
      return "#FFE347";
    case "PURPLE":
      return "#7D7ABC";
    default:
      return colours[Math.floor(Math.random() * colours.length)];
  }
};


//border-left: 20px solid ${(props) => handleColorType(props.color)};
const CardContainer = styled(Paper)`
  display: flex;
  margin: 2em;

  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  transition: 0.3s;
  border-radius: 5px;

  :hover {
    cursor: pointer;
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
  }
`;

const Title = styled.h2`
  display: flex;
  margin: 1em;
  padding: 0.25em 1em;
`;
