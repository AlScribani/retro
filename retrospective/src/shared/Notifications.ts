import { toast, ToastOptions } from "react-toastify";

const toastConfig: ToastOptions = {
  position: "top-right",
  autoClose: 2000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
};

export const notifySuccess = (message: string) =>
  toast.success(message, toastConfig);

export const notifyInfo = (message: string) => toast.info(message, toastConfig);

export const notifyWarning = (message: string) =>
  toast.warn(message, toastConfig);

export const notifyFailure = (error: string, message: string) =>
  toast.error(`${message}: ${error}`, toastConfig);
