import styled from "styled-components";

export const Page = styled.main`
  max-width: 1280px;
  margin: 0 auto;
  @media screen and (max-width: 600px) {
    margin: 10px;
    margin-bottom: 80px;
    margin-top: 30px;
  }
  margin-bottom: 80px;
`;
