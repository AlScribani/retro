import React from "react";
import Spinner from "react-bootstrap/Spinner";
import styled from "styled-components";
import CircularProgress from "@material-ui/core/CircularProgress";

interface LoadingProps {
  size?: string;
}

const Loading = ({ size }: LoadingProps) => (
  <SpinnerContainer className="justify-content-center">
    <CircularProgress size={size} />
  </SpinnerContainer>
);

export default Loading;

const SpinnerContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
