import React, { useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import DrawerContent from "./DrawerContent";
import Typography from "@material-ui/core/Typography";
import Drawer from "@material-ui/core/Drawer";

const NavbarComponent = () => {
  const [isDrawerVisible, setIsDrawerVisible] = useState<boolean>(false);
  const toggleDrawer = (isOpen: boolean) => () => setIsDrawerVisible(isOpen);

  return (
    <AppBar position="sticky" color="primary">
      <Toolbar>
        <LeftContainer>
          <IconButton onClick={toggleDrawer(true)} color="inherit" edge="start">
            <MenuIcon />
          </IconButton>
          <Drawer
            anchor="left"
            open={isDrawerVisible}
            onClose={toggleDrawer(false)}
          >
            <DrawerContent toggleDrawer={toggleDrawer(false)} />
          </Drawer>
          <NavLink title="Home" to="/">
            <Title variant="h6">Global Platform Retrospective</Title>
          </NavLink>
        </LeftContainer>
        <RightContainer>
          <MenuButton edge="end" color="inherit">
            <AccountCircle />
          </MenuButton>
        </RightContainer>
      </Toolbar>
    </AppBar>
  );
};

export default NavbarComponent;

const MenuButton = styled(IconButton)`
  display: flex;
  margin-right: 15px;
`;

const NavLink = styled(Link)`
  display: flex;
  color: inherit;

  &:hover {
    color: inherit;
    text-decoration: none;
  }
`;

const Title = styled(Typography)`
  display: flex;
  align-contnet: center;
  align-items: center;
  justify-content: center;
`;

const LeftContainer = styled.div`
  display: flex;
  align-content: center;
  flex: 1 1 0%;
`;

const RightContainer = styled.div`
  display: block;
`;
