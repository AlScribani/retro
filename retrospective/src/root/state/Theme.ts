import React from "react";
import { createMuiTheme } from "@material-ui/core/styles";

const Theme = createMuiTheme({
  palette: {
    primary: {
      main: "#6457A6",
      dark: "#1a255b",
      light: "#7D7ABC",
    },
    secondary: {
      main: "#19857b",
    },
    error: {
      main: "#EF767A",
    },
    background: {
      default: "#fff",
    },
    success: {
      main: "#23F0C7",
    },
    warning: {
      main: "#FFE347",
    },
  },
});

export default Theme;
