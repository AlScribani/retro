import React, { createContext, useState } from "react";
import { useEffect } from "react";

type UserContextModel = {
  user: string;
  isAuthenticated: boolean;
  setUser: React.Dispatch<React.SetStateAction<string>>;
  setIsAuthenticated: React.Dispatch<React.SetStateAction<boolean>>;
};

type Props = {
  children: React.ReactNode;
};

const defaultContext: UserContextModel = {
  user: "",
  isAuthenticated: false,
  setUser: () => {},
  setIsAuthenticated: () => {},
};

export const UserContext = createContext<UserContextModel>(defaultContext);

export const UserContextProvider = ({ children }: Props) => {
  const [user, setUser] = useState<string>("");
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);

  useEffect(() => {
    if (!user) {
      setIsAuthenticated(false);
    }
  }, [user]);

  const context: UserContextModel = {
    user,
    isAuthenticated,
    setUser,
    setIsAuthenticated,
  };
  
  return (
    <UserContext.Provider value={context}>{children}</UserContext.Provider>
  );
};
