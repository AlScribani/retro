import React from 'react';

interface UserContextProps {
  user: string | null;
  setUser: (user: string | null) => void;
}

const UserContext = React.createContext<UserContextProps>({
  user: null,
  setUser: (_: string | null) => {},
});

export default UserContext;