import { useContext } from "react";
import UserContext from "./UserContext";

function useUser(): string | null {
  const { user } = useContext(UserContext);

  return user;
}

export default useUser;
