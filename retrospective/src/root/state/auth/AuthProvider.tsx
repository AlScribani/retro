import React, { useState, useEffect } from "react";
import Context from "./UserContext";
import { useHistory, useLocation } from "react-router-dom";

const loginRoute = "/login";

const AuthProvider: React.FC = ({ children }) => {
  const localStorageName = localStorage.getItem("name");
  const [user, setUser] = useState<string | null>(localStorageName);
  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    if (location.pathname != loginRoute && !user) {
      history.push(loginRoute);
    }
  }, [location]);

  return (
    <Context.Provider value={{ setUser, user }}>{children}</Context.Provider>
  );
};

export default AuthProvider;
