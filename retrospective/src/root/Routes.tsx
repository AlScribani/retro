import { Switch, Route } from "react-router-dom";
import HomeComponent from "../home/HomeComponent";
import TeamComponent from "../team/Team";
import TeamsListComponent from "../team/TeamsListComponent";
import SprintComponent from "../sprint/Sprint";
import LoginComponent from "../login/Login";
import {
  ProtectedRoute,
  ProtectedRouteProps,
} from "./state/auth/ProtectedRoute";
import { useContext } from "react";
import UserContext from "./state/auth/UserContext";

// <ProtectedRoute
//   {...defaultProtectedRouteProps}
//   exact={true}
//   path="/"
//   component={ProtectedContainer}
// />;

const Routes = () => {
  const { user } = useContext(UserContext);

  return (
    <Switch>
      <Route exact path="/" component={HomeComponent} />
      <Route exact path="/login" component={LoginComponent} />
      <Route exact path="/teams" component={TeamsListComponent} />
      <Route exact path="/:teamId" component={TeamComponent} />
      <Route exact path="/:teamId/:sprintId" component={SprintComponent} />
    </Switch>
  );
};

export default Routes;
