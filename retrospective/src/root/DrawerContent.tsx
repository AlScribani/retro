import React, { useState } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import styled from "styled-components";
import IconDashboard from "@material-ui/icons/Dashboard";
import IconLibraryBooks from "@material-ui/icons/LibraryBooks";
import IconExpandMore from "@material-ui/icons/ExpandMore";
import IconExpandLess from "@material-ui/icons/ExpandLess";
import Collapse from "@material-ui/core/Collapse";
import Divider from "@material-ui/core/Divider";
import { useHistory } from "react-router-dom";
import useApi from "../api/shared/UseApi";
import * as teamsApi from "../api/handlers/TeamsApi";
import { Team } from "../api/types/Team";
import Loading from "../shared/Loading";

interface DrawerContentProps {
  toggleDrawer: () => void;
}

const DrawerContent = ({ toggleDrawer }: DrawerContentProps) => {
  const getTeams = useApi<Team[]>({
    action: () => teamsApi.getTeams(),
    defer: true,
  });
  const history = useHistory();
  const [open, setOpen] = useState<boolean>(false);

  const handleClick = () => {
    setOpen(!open);
    getTeams.execute();
  };

  const handleLinkClick = (link: string) => {
    toggleDrawer();
    history.push(link);
  };
  return (
    <DrawerContainer>
      <ContentList disablePadding>
        <ContentListItem button onClick={() => handleLinkClick("/")}>
          <ContentItemIcon>
            <IconDashboard />
          </ContentItemIcon>
          <ListItemText primary="Home" />
        </ContentListItem>
        <ListItem button onClick={handleClick}>
          <ListItemIcon>
            <IconLibraryBooks />
          </ListItemIcon>
          <ListItemText primary="Teams" />
          {open ? <IconExpandLess /> : <IconExpandMore />}
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <Divider />
          <List component="div" disablePadding>
            {getTeams.inProgress ? (
              <Loading />
            ) : (
              getTeams.data?.map((team, index) => {
                console.log("team", team);
                return (
                  <NestedListItem
                    colour={team.colour}
                    key={index}
                    button
                    onClick={() => handleLinkClick(team.id)}
                  >
                    <ListItemText primary={team.name} />
                  </NestedListItem>
                );
              })
            )}
          </List>
        </Collapse>
      </ContentList>
    </DrawerContainer>
  );
};

export default DrawerContent;

const handleColorType = (color: string) => {
  switch (color.toUpperCase()) {
    case "RED":
      return "#EF767A";
    case "GREEN":
      return "#23F0C7";
    case "YELLOW":
      return "#FFE347";
    case "PURPLE":
      return "#6457A6";
    default:
      return "#7D7ABC";
  }
};

const NestedListItem = styled(ListItem)<{
  colour: string;
}>`
  display: flex;
  padding-left: 24px;
  background-colour: light-gray;
  border-left: 15px solid;
  border-color: ${(props) => handleColorType(props.colour)};
`;

const DrawerContainer = styled.div`
  display: flex;
  width: 240px;
`;

const ContentItemIcon = styled(ListItemIcon)`
  color: primary;
`;

const ContentList = styled(List)`
  width: 100%;
`;

const ContentListItem = styled(ListItem)`
  width: 240px;
`;
