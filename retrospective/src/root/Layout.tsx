import React from "react";
import NavbarComponent from "./Navbar";
import Routes from "./Routes";
import Container from "react-bootstrap/Container";
import { BrowserRouter as Router } from "react-router-dom";
import styled from "styled-components";
import AuthProvider from "./state/auth/AuthProvider";

const Layout = () => {
  return (
    <Router>
      <AuthProvider>
        <NavbarComponent />
        <Routes />
      </AuthProvider>
    </Router>
  );
};

export default Layout;
