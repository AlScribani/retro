import React from "react";
import { Tooltip, Button } from "@material-ui/core";
import styled from "styled-components";

interface ActionButtonProps {
  tooltip: React.ReactElement | string | number;
  icon: JSX.Element;
  disabled?: boolean;
  innerRef?: React.RefObject<HTMLButtonElement>;
  onClick?: (event: any) => void;
}

const ActionButton: React.FC<ActionButtonProps> = ({
  tooltip,
  icon,
  onClick,
  disabled,
  children,
}) => {
  const showTooltip = !!tooltip;
  return (
    <Tooltip
      placement="bottom"
      disableHoverListener={!showTooltip}
      disableFocusListener={!showTooltip}
      disableTouchListener={!showTooltip}
      title={tooltip}
    >
      <ActionButtonContainer>
        <StyledButton
          onClick={onClick}
          disabled={!!disabled}
          tabIndex={-1}
          style={{
            position: "relative",
            paddingLeft: children !== undefined ? 10 : 0,
            paddingRight: children !== undefined ? 10 : 0,
            minWidth: children !== undefined ? 64 : 42,
          }}
        >
          {icon}
          {children !== undefined ? (
            <ChildrenContainer>&nbsp;{children}</ChildrenContainer>
          ) : null}
        </StyledButton>
      </ActionButtonContainer>
    </Tooltip>
  );
};

export default ActionButton;

const ActionButtonContainer = styled.span`
  min-width: 64px;
  margin: 0 3px;
`;

const ChildrenContainer = styled.span`
  //   padding: 0 5px;
`;

const StyledButton = styled(Button)`
  line-height: 1px;
`;
