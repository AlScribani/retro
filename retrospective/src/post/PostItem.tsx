import React, { useContext, useState } from "react";
import styled from "styled-components";
import { Post } from "../api/types/Post";
import { Card, CardContent, CardActions } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import ActionsBar from "./ActionsBar";
import useApi from "../api/shared/UseApi";
import * as postsApi from "../api/handlers/PostsApi";
import { AxiosResponse } from "axios";
import UserContext from "../root/state/auth/UserContext";

interface PostProps {
  post: Post;
  onDelete: (postId: string) => void;
}

const PostItem = ({ post, onDelete }: PostProps) => {
  const { user } = useContext(UserContext);
  const [userHasLiked, setUserHasLiked] = useState<boolean>(
    post.likes.some((like) => like === user)
  );
  const [likes, setLikes] = useState<string[]>(post.likes);
  const likePost = useApi(
    {
      action: () => postsApi.likePost(post.id, user!),
      defer: true,
      onSuccess: (result: AxiosResponse<string>) => {
        setLikes(post.likes.concat(result.data));
        setUserHasLiked(true);
      },
    },
    [likes]
  );
  const unlikePost = useApi(
    {
      action: () => postsApi.unlikePost(post.id, user!),
      defer: true,
      onSuccess: (result: AxiosResponse<string>) => {
        setLikes(post.likes.filter((userId) => userId != result.data));
        setUserHasLiked(false);
      },
    },
    [likes]
  );
  return (
    <PostCard elevation={3}>
      <CardContent>
        <LabelContainer>
          <Typography align="left" variant="body1">
            {post.content}
          </Typography>
        </LabelContainer>
        <AuthorContainer>
          <Typography variant="caption" color="textSecondary" component="div">
            by&nbsp;
          </Typography>
          <Typography variant="caption" color="textPrimary" component="div">
            {post.userId}
            {/* {post.userId === user ? "you" : post.userId} */}
          </Typography>
        </AuthorContainer>
      </CardContent>
      <ActionsBar
        onDeletePost={() => onDelete(post.id)}
        onLikePost={() => likePost.execute(post.id)}
        onUnlikePost={() => unlikePost.execute(post.id)}
        userHasLiked={userHasLiked}
        likes={likes.length}
        colour={post.type}
        canDelete={post.userId === user}
      />
    </PostCard>
  );
};

export default PostItem;

const PostCard = styled(Card)`
  margin: 10px 5px;
  margin-bottom: 20px;
  position: relative;
`;

const LabelContainer = styled.div`
  display: flex;
`;

const AuthorContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  position: relative;
  margin-top: -10px;
  top: 10px;
  right: -5px;
`;

// const PostCard = styled(Card)`
//   margin: 10px 5px;
//   margin-bottom: 20px;
//   position: relative;

//   :hover {
//     ${DragHandle} {
//       visibility: visible;
//     }
//   }
// `;
