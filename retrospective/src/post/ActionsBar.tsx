import React from "react";
import styled from "styled-components";
import { CardActions } from "@material-ui/core";
import { AiOutlineLike, AiFillLike } from "react-icons/ai";
import { BiDotsVerticalRounded } from "react-icons/bi";
import { FaRegComment } from "react-icons/fa";
import ActionButton from "./ActionButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Divider from "@material-ui/core/Divider";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { RiDeleteBin5Line } from "react-icons/ri";

interface ActionsBarProps {
  likes: number;
  colour: string;
  userHasLiked: boolean;
  canDelete: boolean;
  onLikePost: () => void;
  onUnlikePost: () => void;
  onDeletePost: () => void;
}

const ActionsBar = ({
  likes,
  colour,
  userHasLiked,
  canDelete,
  onLikePost,
  onUnlikePost,
  onDeletePost,
}: ActionsBarProps) => {
  const [anchorEl, setAnchorEl] = React.useState<HTMLInputElement | null>(null);
  const handleClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleDeletePost = () => {
    handleClose();
    onDeletePost();
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <Actions style={{ backgroundColor: `${handleColorType(colour)}` }}>
      <ButtonsContainer>
        <MainButtons>
          <ActionButton
            tooltip={userHasLiked ? "Unlike" : "Like"}
            disabled={false}
            icon={
              userHasLiked ? (
                <AiFillLike size="24" />
              ) : (
                <AiOutlineLike size="24" />
              )
            }
            onClick={userHasLiked ? onUnlikePost : onLikePost}
            children={likes}
          />
          <ActionButton
            tooltip="Comment"
            disabled={false}
            icon={<FaRegComment size="24" />}
            onClick={() => {}}
            children="Comment"
          />
        </MainButtons>
        <RightActions>
          <ActionButton
            tooltip="More"
            disabled={false}
            icon={<BiDotsVerticalRounded size="24" />}
            onClick={handleClick}
          />
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            // style={{ paddingBottom: 0 }}
          >
            <MenuItem onClick={handleClose}>My account</MenuItem>
            <MenuItem onClick={handleClose}>Logout</MenuItem>
            {canDelete && (
              <DeleteButtonContainer>
                <Divider />
                <MenuItem onClick={handleDeletePost}>
                  <DeleteIconContainer>
                    <RiDeleteBin5Line size="24" />
                  </DeleteIconContainer>
                  <DeleteText>Delete</DeleteText>
                </MenuItem>
              </DeleteButtonContainer>
            )}
          </Menu>
        </RightActions>
      </ButtonsContainer>
    </Actions>
  );
};

export default ActionsBar;

const DeleteButtonContainer = styled.div``;

const DeleteIconContainer = styled(ListItemIcon)`
  min-width: 30px;
`;

const DeleteText = styled(ListItemText)`
  display: flex;
`;

const handleColorType = (color: string) => {
  switch (color.toUpperCase()) {
    case "BAD":
      return "#EF767A";
    case "GOOD":
      return "#23F0C7";
    case "IMPROVE":
      return "#FFE347";
    default:
      return "#7D7ABC";
  }
};

const Actions = styled(CardActions)`
  position: relative;
  flex: 1;
  padding: 0 !important;
  > div {
    flex: 1;
    margin: 0;
  }
`;

const ButtonsContainer = styled.div`
  display: flex;
  margin: 0;
  padding: 8px;
`;

const RightActions = styled.div`
  display: block;
`;

const MainButtons = styled.div`
  display: flex;
  flex: 1 1 0%;
`;
