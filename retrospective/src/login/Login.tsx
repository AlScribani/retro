import React, { useState, useCallback, useContext } from "react";
import { Dialog, DialogContent, AppBar, Tabs, Tab } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import Guest from "./dialogs/Guest";
import Jira from "./dialogs/Jira";
import Internal from "./dialogs/Internal";
import UserContext from "../root/state/auth/UserContext";

const Login = () => {
  const history = useHistory();
  const { setUser } = useContext(UserContext);
  const [open, setOpen] = useState(true);
  const handleClose = () => {
    setOpen(false);
  };
  const onClose = useCallback(() => {
    history.push("/");
  }, [history]);

  const [currentTab, setCurrentTab] = useState("guest");
  const handleTab = useCallback((_: React.ChangeEvent<{}>, value: string) => {
    setCurrentTab(value);
  }, []);

  return (
    <Dialog disableBackdropClick fullWidth open={open} onClose={handleClose}>
      <AppBar position="static" color="default">
        <Tabs
          value={currentTab}
          onChange={handleTab}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
        >
          <Tab label="Jira" value="jira" />
          <Tab label="Retrospective Account" value="account" />
          <Tab label="Guest" value="guest" />
        </Tabs>
      </AppBar>
      <DialogContent>
        {currentTab === "jira" && <Jira onClose={onClose} onUser={setUser} />}
        {currentTab === "account" && (
          <Internal onClose={onClose} onUser={setUser} />
        )}
        {currentTab === "guest" && <Guest onClose={onClose} onUser={setUser} />}
      </DialogContent>
    </Dialog>
  );
};

export default Login;
