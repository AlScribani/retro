import React, { useState, useCallback } from "react";
import Wrapper from "../Wrapper";
import Alert from "@material-ui/lab/Alert";
import { Button, Input } from "@material-ui/core";
import styled from "styled-components";

interface InternalProps {
  onClose: () => void;
  onUser: (name: string) => void;
}

const Internal = ({ onClose, onUser }: InternalProps) => {
  const [name, setName] = useState<string>("");
  const handleUsernameChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => setName(e.target.value),
    [setName]
  );
  return (
    <Wrapper
      header="Internal Login"
      actions={
        <Button
        //   onClick={handleAccountogin}
          color="primary"
          autoFocus
          //   startIcon={loading ? <CircularProgress size="1em" /> : null}
          //   disabled={!loginEmail || !loginPassword || loading}
        >
          LOGIN
        </Button>
      }
    >
      <Alert severity="info">Login with your email and password.</Alert>
      {/* {!!error ? (
        <Alert severity="error" style={{ marginTop: 10 }}>
          {error}
        </Alert>
      ) : null} */}
      <Input
        // value={loginEmail}
        // onChangeValue={setLoginEmail}
        title="Email"
        placeholder="Email"
        type="email"
        fullWidth
        style={{ marginTop: 20 }}
        // leftIcon={<Email />}
      />
      <Input
        // value={loginPassword}
        // onChangeValue={setLoginPassword}
        title="Password"
        placeholder="Password"
        type="password"
        fullWidth
        style={{ marginTop: 20 }}
        // leftIcon={<VpnKey />}
      />
      <div style={{ marginTop: 20 }} />
      <Links>
        {/* <Link onClick={onAskRegistration}>{translations.registerLink}</Link>
        <Link onClick={onAskPasswordReset}>
          {translations.forgotPasswordLink}
        </Link> */}
      </Links>
    </Wrapper>
  );
};

export default Internal;

const Links = styled.div`
  display: flex;
  > :first-child {
    margin-right: 20px;
  }
  @media (max-width: 400px) {
    flex-direction: column;
    > :first-child {
      margin-right: 0px;
      margin-bottom: 5px;
    }
  }
`;
