import React, { useState, useCallback } from "react";
import Wrapper from "../Wrapper";
import Alert from "@material-ui/lab/Alert";
import { Button, Input } from "@material-ui/core";

interface JiraProps {
  onClose: () => void;
  onUser: (name: string) => void;
}

const Jira = ({ onClose, onUser }: JiraProps) => {
  const [name, setName] = useState<string>("");
  const handleUsernameChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => setName(e.target.value),
    [setName]
  );
  return (
    <Wrapper
      header="Jira Login"
      actions={
        <Button
          color="primary"
          variant="contained"
          //   onClick={login.execute}
          //   disabled={!email || login.inProgress}
          //   startIcon={login.inProgress && <Loading />}
        >
          LOGIN
        </Button>
      }
    >
      <Alert severity="info">
        This will use a third party provider of your choosing to authenticate
        you. No password is stored.
      </Alert>
      <Input
        value={name}
        onChange={handleUsernameChange}
        // title={loginTranslations.buttonLabel}
        placeholder="Please enter a name here to continue"
        fullWidth
        style={{ marginTop: 20 }}
      />
    </Wrapper>
  );
};

export default Jira;
