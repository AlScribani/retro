import React, { useState, useCallback } from "react";
import Wrapper from "../Wrapper";
import Alert from "@material-ui/lab/Alert";
import { Button, Input } from "@material-ui/core";
import Loading from "../../shared/Loading";
import * as usersApi from "../../api/handlers/UsersApi";

interface GuestProps {
  onClose: () => void;
  onUser: (user: string | null) => void;
}

const Guest = ({ onClose, onUser }: GuestProps) => {
  const [name, setName] = useState<string>("");
  const [inProgress, setInProgress] = useState(false);
  const handleAnonLogin = () => {
    setInProgress(true);
    if (name.length) {
      usersApi.guestLogin(name, onUser);
    }
    setInProgress(false);
    if (onClose) {
      onClose();
    }
  };
  const handleUsernameChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => setName(e.target.value),
    [setName]
  );
  return (
    <Wrapper
      header="Guest Login"
      actions={
        <Button
          color="primary"
          variant="contained"
          autoFocus
          onClick={handleAnonLogin}
          disabled={!name || inProgress}
          startIcon={inProgress && <Loading size="1em" />}
        >
          LOGIN
        </Button>
      }
    >
      <Alert severity="info">
        This will create an guest account. Some features won't be available.
      </Alert>
      <Input
        value={name}
        onChange={handleUsernameChange}
        title="Name"
        placeholder="Please enter a name here to continue"
        fullWidth
        style={{ marginTop: 20 }}
      />
    </Wrapper>
  );
};

export default Guest;
