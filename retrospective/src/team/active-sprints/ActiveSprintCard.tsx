import React, { useState } from "react";
import styled from "styled-components";
import StyledLink from "../../shared/StyledLink";
import CardBase from "react-bootstrap/Card";
import ItemStat from "./StatItem";
import { getInitials } from "../../shared/Helpers";
import { Sprint } from "../../api/types/Sprint";
import useApi from "../../api/shared/UseApi";
import * as postsApi from "../../api/handlers/PostsApi";
import { Post } from "../../api/types/Post";
import Loading from "../../shared/Loading";
import { AxiosResponse } from "axios";
import Avatar from "@material-ui/core/Avatar";
import AvatarGroup from "@material-ui/lab/AvatarGroup";

interface ActiveSprintCardProps {
  sprint: Sprint;
  linkTo: string;
}

const ActiveSprintCard = ({ sprint, linkTo }: ActiveSprintCardProps) => {
  const [uniqueParticipants, setUniqueParticipants] = useState<string[]>([]);
  const getPosts = useApi<Post[]>({
    action: () => postsApi.getPosts(sprint.id),
    onSuccess: (result: AxiosResponse<Post[]>) => {
      setUniqueParticipants(
        result.data.map((x) => x.userId).filter(onlyUnique)
      );
    },
  });

  const getTotalLikesCount = (likesArray: string[][]) => {
    if (likesArray == null) return 0;
    let totalLikesCount: number = 0;
    likesArray.forEach((likes) => {
      totalLikesCount = totalLikesCount + likes.length;
    });
    return totalLikesCount;
  };

  const onlyUnique = (value: any, index: number, self: any) =>
    self.indexOf(value) === index;

  return (
    <StyledLink to={linkTo}>
      <Card>
        <CardBase.Body>
          <Top>
            <LastUpdated>Last updated {sprint.lastUpdatedAt}</LastUpdated>
          </Top>
          <NameContainer>
            <Name>{sprint.name}</Name>
          </NameContainer>
          <CreatedBy>Created by {sprint.createdBy}</CreatedBy>
          {getPosts.inProgress ? (
            <Loading />
          ) : (
            <Stats>
              <ItemStat
                value={getPosts.data?.length!}
                label="post"
                color={"#23F0C7"}
              />
              <ItemStat
                value={uniqueParticipants.length}
                label="participant"
                color={"#7D7ABC"}
              />
              <ItemStat
                value={getTotalLikesCount(getPosts.data?.map((x) => x.likes)!)}
                label="like"
                color={"#EF767A"}
              />
              <ItemStat
                value={Math.floor(
                  Math.random() + Math.random() + Math.random()
                )}
                label="action"
                color={"#FFE347"}
              />
            </Stats>
          )}
          <ParticipantsContainer>
            <AvatarGroup max={5}>
              {uniqueParticipants.map((user, index) => (
                <Avatar key={index} title={user}>
                  {getInitials(user)}
                </Avatar>
              ))}
            </AvatarGroup>
          </ParticipantsContainer>
        </CardBase.Body>
      </Card>
    </StyledLink>
  );
};

export default ActiveSprintCard;

const ParticipantsContainer = styled.div`
  display: flex;
`;

const Card = styled(CardBase)`
  color: rgba(0, 0, 0, 0.87);
  transition: box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  background-color: #fff;
  width: 500px;
  cursor: pointer;
  border-radius: 4px;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);

  &:hover {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
  }

  @media screen and (max-width: 500px) {
    width: calc(100vw - 40px);
    font-size: 0.5em;
  }
`;

const Top = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 20px;
`;

const Stats = styled.div`
  display: flex;
  justify-content: space-evenly;
  background-color: rgb(245, 245, 245);
  margin: 0 -20px 20px;
`;

const Name = styled.h2`
  font-size: 1.5rem;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  font-weight: 400;
  line-height: 1.334;
  letter-spacing: 0em;
`;

const CreatedBy = styled.div`
  display: flex;
  margin-bottom: 20px;
  color: rgba(0, 0, 0, 0.54);
  font-size: 1rem;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  font-weight: 400;
  line-height: 1.5;
  letter-spacing: 0.00938em;
`;

const LastUpdated = styled.div`
  display: flex;
  flex: 1;
`;

const NameContainer = styled.div`
  display: flex;
  align-items: center;
`;
