import React, { useContext } from "react";
import { useParams, useHistory } from "react-router-dom";
import styled from "styled-components";
import { Page } from "../shared/Page";
import useApi from "../api/shared/UseApi";
import * as sprintsApi from "../api/handlers/SprintsApi";
import * as teamsApi from "../api/handlers/TeamsApi";
import { Team } from "../api/types/Team";
import { Sprint } from "../api/types/Sprint";
import Loading from "../shared/Loading";
import { AxiosResponse } from "axios";
import ActiveSprintCard from "./active-sprints/ActiveSprintCard";
import List from "./completed-sprints/List";
import Button from "@material-ui/core/Button";
import StyledLink from "../shared/StyledLink";
import { notifyFailure, notifySuccess } from "../shared/Notifications";
import UserContext from "../root/state/auth/UserContext";

interface RouteParams {
  teamId: string;
}

const TeamComponent = () => {
  const { user } = useContext(UserContext);
  const { teamId } = useParams<RouteParams>();
  const history = useHistory();

  const getTeam = useApi<Team>({
    action: () => teamsApi.getTeam(teamId),
  });

  const getSprints = useApi<Sprint[]>({
    action: () => sprintsApi.getSprintsByTeam(teamId),
    condition: !!teamId,
  });

  const createSprint = useApi<string>({
    action: () => sprintsApi.createSprint(teamId, user!),
    defer: true,
    onSuccess: (result: AxiosResponse<string>) =>
      history.push(`/${teamId}/${result.data}`),
  });
  const deleteSprint = useApi({
    action: (sprintId: string) => sprintsApi.deleteSprint(sprintId),
    defer: true,
    onSuccess: (result: AxiosResponse<string>) => {
      notifySuccess("Sprint deleted");
      getSprints.setData(
        getSprints.data?.filter((sprint) => sprint.id != result.data)!
      );
    },
    onError: (e: string) => notifyFailure(e, "Failed to delete sprint"),
  });

  return (
    <Page>
      <Header>
        <HeaderText>
          {getTeam.inProgress ? <Loading /> : getTeam.data?.name}
        </HeaderText>
        <ButtonContainer>
          <StyledButton
            variant="contained"
            color="primary"
            disableElevation
            onClick={createSprint.execute}
          >
            Create Sprint
          </StyledButton>
          <StyledLink to="/teams">
            <StyledButton
              variant="contained"
              color="secondary"
              disableElevation
            >
              Back to teams
            </StyledButton>
          </StyledLink>
        </ButtonContainer>
      </Header>
      <ActiveSprintsContainer>
        {getSprints.inProgress ? (
          <Loading />
        ) : (
          getSprints.data?.map(
            (sprint, index) =>
              !sprint.isComplete && (
                <ActiveSprintCard
                  key={index}
                  sprint={sprint}
                  linkTo={`/${teamId}/${sprint.id}`}
                />
              )
          )
        )}
      </ActiveSprintsContainer>
      <SubHeader>Completed Sprints</SubHeader>
      {getSprints.inProgress ? (
        <Loading />
      ) : (
        getSprints.data && (
          <List
            sprints={getSprints.data?.filter((x) => x.isComplete)!}
            onDelete={deleteSprint.execute}
          />
        )
      )}
    </Page>
  );
};

export default TeamComponent;

const ActiveSprintsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 50px;
  > * {
    margin: 20px;
  }
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 4rem 0;
`;

const HeaderText = styled.h1`
  font-weight: 100;
  font-size: 4em;
  @media screen and (max-width: 500px) {
    font-size: 2em;
  }
`;

const SubHeader = styled.h2`
  display: flex;
  font-weight: 100;
  font-size: 3em;
  @media screen and (max-width: 500px) {
    font-size: 1.5em;
  }
`;

const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  margin: 0.25em;
`;

const StyledButton = styled(Button)`
  display: flex;
  margin: 0 5px;
`;
