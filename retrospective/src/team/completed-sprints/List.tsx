import React from "react";
import { Sprint } from "../../api/types/Sprint";
import AccordionDetailsComponent from "./AccordionDetails";
import MuiAccordion from "@material-ui/core/Accordion";
import MuiAccordionSummary from "@material-ui/core/AccordionSummary";
import MuiAccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import styled from "styled-components";

interface ListProps {
  sprints: Sprint[];
  onDelete: () => void;
}

const List = ({ sprints, onDelete }: ListProps) => {
  const [expanded, setExpanded] = React.useState(null);
  const handleChange = (panel: any) => (_event: any, newExpanded: any) => {
    setExpanded(newExpanded ? panel : false);
  };
  return (
    <div>
      {sprints.map((sprint, index) => (
        <Accordion
          key={index}
          square
          expanded={expanded === index}
          onChange={handleChange(index)}
        >
          <AccordionSummary>
            <Typography variant="h6">{sprint.name}</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <AccordionDetailsComponent
              sprintId={sprint.id}
              createdBy={sprint.createdBy}
              isSprintComplete={sprint.isComplete}
              onDelete={onDelete}
            />
          </AccordionDetails>
        </Accordion>
      ))}
    </div>
  );
};

export default List;

const Accordion = styled(MuiAccordion)`
  border: 1px solid rgba(0, 0, 0, 0.125);
  box-shadow: none;

  &:not(:last-child): {
    border-bottom: 0;
  }
  &:before: {
    display: none;
  }
  &.expanded: {
    margin: auto;
  }
  expanded: {
  }
`;

const AccordionSummary = styled(MuiAccordionSummary)`
  background-color: rgba(0, 0, 0, 0.03);
  border-bottom: 1px solid secondary;
  margin-bottom: -1px;
  min-height: 48px;

  &.expanded: {
    min-height: 56px;
  }
`;

const AccordionDetails = styled(MuiAccordionDetails)`
  display: flex;
  padding: 16px;
`;
