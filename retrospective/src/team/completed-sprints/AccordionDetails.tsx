import React, { useState } from "react";
import { useLocation, useHistory } from "react-router-dom";
import styled from "styled-components";
import ActionButton from "../../post/ActionButton";
import { GrOverview } from "react-icons/gr";
import { FaLockOpen } from "react-icons/fa";
import { RiDeleteBin5Line } from "react-icons/ri";
import StyledLink from "../../shared/StyledLink";
import Typography from "@material-ui/core/Typography";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import useApi from "../../api/shared/UseApi";
import * as sprintsApi from "../../api/handlers/SprintsApi";
import { notifyFailure, notifySuccess } from "../../shared/Notifications";

interface AccordionDetailsProps {
  sprintId: string;
  createdBy: string;
  isSprintComplete: boolean;
  onDelete: (sprintId: string) => void;
}

const AccordionDetails = ({
  sprintId,
  createdBy,
  isSprintComplete,
  onDelete,
}: AccordionDetailsProps) => {
  let location = useLocation();
  let history = useHistory();
  const [openDialog, setOpenDialog] = useState(false);
  const handleClickOpen = () => {
    setOpenDialog(true);
  };
  const handleClose = () => {
    setOpenDialog(false);
  };
  const reactivateSprint = useApi({
    action: () => sprintsApi.reactivateSprint(sprintId),
    defer: true,
    onSuccess: () => {
      handleClose();
      history.push(`${location.pathname}/${sprintId}`);
    },
  });
  const deleteSprint = () => {
    onDelete(sprintId);
    handleClose();
  };
  return (
    <Details>
      <Typography>
        <CreatedBy>Created by {createdBy}</CreatedBy>
      </Typography>
      <ButtonsContainer>
        <LeftButtonsContainer>
          <StyledLink to={`${location.pathname}/${sprintId}`}>
            <ActionButton
              tooltip="View sprint"
              icon={<GrOverview size="24" />}
              children="View"
            />
          </StyledLink>
          <ActionButton
            onClick={reactivateSprint.execute}
            tooltip="Re-open sprint"
            icon={<FaLockOpen size="24" />}
            children="Re-open sprint"
          />
        </LeftButtonsContainer>
        <RightButtonsContainer>
          <ActionButton
            disabled={!isSprintComplete}
            onClick={handleClickOpen}
            tooltip="Delete sprint"
            icon={<RiDeleteBin5Line size="24" />}
            children="Delete sprint"
          />
          <Dialog open={openDialog} onClose={handleClose}>
            <DialogTitle>
              {"Are you sure you want to delete this sprint?"}
            </DialogTitle>
            <DialogContent>
              <DialogContentText>
                This action cannot be undone and all posts related to this
                sprint will be deleted.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="default">
                Cancel
              </Button>
              <Button
                variant="contained"
                style={{ backgroundColor: "#E41B21", color: "#FFF" }}
                onClick={() => deleteSprint()}
                autoFocus
                disabled={reactivateSprint.inProgress}
              >
                Delete
              </Button>
            </DialogActions>
          </Dialog>
        </RightButtonsContainer>
      </ButtonsContainer>
    </Details>
  );
};

export default AccordionDetails;

const CreatedBy = styled.div`
  display: flex;
  margin-bottom: 20px;
  color: rgba(0, 0, 0, 0.54);
  font-size: 1rem;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  font-weight: 400;
  line-height: 1.5;
  letter-spacing: 0.00938em;
`;

const Details = styled.div`
  position: relative;
  flex: 1;
  padding: 0 !important;
  > div {
    flex: 1;
    margin: 0;
  }
`;

const ButtonsContainer = styled.div`
  display: flex;
  margin: 0;
  padding: 8px;
`;

const LeftButtonsContainer = styled.div`
  display: flex;
  flex: 1 1 0%;
`;

const RightButtonsContainer = styled.div`
  display: block;
`;
