import React from "react";
import TeamCard from "../shared/TeamCard";
import CardDeck from "react-bootstrap/CardDeck";
import StyledLink from "../shared/StyledLink";
import styled from "styled-components";
import { Page } from "../shared/Page";
import useApi from "../api/shared/UseApi";
import * as teamsApi from "../api/handlers/TeamsApi";
import Loading from "../shared/Loading";
import { Team } from "../api/types/Team";

const TeamsListComponent = () => {
  const getTeams = useApi<Team[]>({
    action: () => teamsApi.getTeams(),
  });

  return (
    <TeamsListComponentPage>
      {getTeams.inProgress ? (
        <Loading />
      ) : (
        getTeams.data?.map((team, index) => (
          <StyledLink
            key={index}
            to={`/${team.id}`}
            title={`View ${team.name} sprints`}
          >
            <CardDeck>
              <TeamCard title={team.name} colour={team.colour} />
            </CardDeck>
          </StyledLink>
        ))
      )}
    </TeamsListComponentPage>
  );
};

export default TeamsListComponent;

const TeamsListComponentPage = styled(Page)`
  padding: 4em 0;
`;
