import "./App.scss";
import "react-toastify/dist/ReactToastify.css";
import React from "react";
import Layout from "./root/Layout";
import { ToastContainer } from "react-toastify";
import { ThemeProvider } from "@material-ui/styles";
import Theme from "./root/state/Theme";

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={Theme}>
          <ToastContainer />
          <Layout />
      </ThemeProvider>
    </div>
  );
}

export default App;
