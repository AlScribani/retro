import React from "react";
import { useParams } from "react-router-dom";
import Board from "./board/Board";
import Summary from "./Summary";
import styled from "styled-components";
import StyledLink from "../shared/StyledLink";
import { BiArrowBack } from "react-icons/bi";
import { AiOutlineUnorderedList, AiOutlineProject } from "react-icons/ai";
import TabsBase from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Button from "@material-ui/core/Button";

interface RouteParams {
  teamId: string;
  sprintId: string;
}

interface TabPanelProps {
  children: any;
  value: number;
  index: number;
}

const TabPanel = ({ children, value, index, ...other }: TabPanelProps) => {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && <div>{children}</div>}
    </div>
  );
};

const SprintComponent = () => {
  const { teamId, sprintId } = useParams<RouteParams>();
  const [value, setValue] = React.useState<number>(2);

  const handleChange = (_event: any, newValue: number) => {
    setValue(newValue);
  };

  return (
    <div>
      <Tabs value={value} onChange={handleChange}>
        <BackLink to={`/${teamId}`} title="Back">
          <BackTab icon={<BiArrowBack size={32} />} />
        </BackLink>
        <VerticalDivider />
        <Tab icon={<AiOutlineProject size={28} />} label="Board" />
        <Tab icon={<AiOutlineUnorderedList size={28} />} label="Summary" />
      </Tabs>
      <TabPanel value={value} index={2}>
        <Board />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <Summary />
      </TabPanel>
    </div>
  );
};

export default SprintComponent;

const VerticalDivider = styled.div`
  diplay: flex;
  border-right: 1px solid lightgray;
  margin: 10px 0;
`;

const Tabs = styled(TabsBase)`
  display: flex;
  color: rgba(0, 0, 0, 0.87);
  background-color: #f5f5f5;
  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2),
    0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12);
  width: 100%;
  height: 80px;
  z-index: 1100;
  box-sizing: border-box;
  flex-shrink: 0;
`;

const BackTab = styled(Tab)`
  display: flex;
  justify-content: center;
  align-items: center;
  transition-duration: 0.3s;
  color: #6457a6;

  &:hover,
  &:focus,
  &:active {
    transform: translateX(-5px);
    color: #7d7abc;
  }
`;

const BackLink = styled(StyledLink)`
  display: flex;
`;
