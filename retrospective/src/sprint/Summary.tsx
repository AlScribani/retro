import React from "react";
import styled from "styled-components";

const Summary = () => {
  return <SummaryContainer>Summary</SummaryContainer>;
};

export default Summary;

const SummaryContainer = styled.div`
  margin: 50px 50px 80px;
`;
