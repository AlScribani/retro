import "../../assets/colours.scss";
import React, { useContext } from "react";
import styled from "styled-components";
import Column from "./Column";
import { Sprint } from "../../api/types/Sprint";
import { Post } from "../../api/types/Post";
import useApi from "../../api/shared/UseApi";
import * as sprintsApi from "../../api/handlers/SprintsApi";
import * as postsApi from "../../api/handlers/PostsApi";
import { useParams } from "react-router-dom";
import Loading from "../../shared/Loading";
import { AxiosResponse } from "axios";
import Button from "@material-ui/core/Button";
import {
  FaRegCheckCircle,
  FaRegTimesCircle,
  FaRegLightbulb,
  FaCheckCircle,
} from "react-icons/fa";
import { FiCheckCircle } from "react-icons/fi";
import { notifySuccess } from "../../shared/Notifications";
import CircularProgress from "@material-ui/core/CircularProgress";
import classname from "classnames";
import UserContext from "../../root/state/auth/UserContext";

interface RouteParams {
  teamId: string;
  sprintId: string;
}

const Board = () => {
  const { user } = useContext(UserContext);
  const { teamId, sprintId } = useParams<RouteParams>();
  const getSprint = useApi<Sprint>({
    action: () => sprintsApi.getSprint(teamId, sprintId),
    defer: false,
  });
  const getPosts = useApi<Post[]>({
    action: () => postsApi.getPosts(sprintId),
  });

  interface AddPostProps {
    content: string;
    type: string;
  }

  const addPost = useApi<Post>(
    {
      action: (newPost: AddPostProps) =>
        postsApi.addPost(sprintId, newPost.content, newPost.type, user!),
      defer: true,
      onSuccess: (result: AxiosResponse<Post>) => {
        getPosts.setData(getPosts.data?.concat(result.data)!);
      },
    },
    [getPosts]
  );

  const deletePost = useApi(
    {
      action: (postId) => postsApi.deletePost(postId),
      defer: true,
      onSuccess: (result: AxiosResponse<string>) => {
        getPosts.setData(
          getPosts.data?.filter((post) => post.id != result.data)!
        );
        notifySuccess("Post deleted");
      },
    },
    [getPosts]
  );

  const completeSprint = useApi({
    action: () => sprintsApi.completeSprint(sprintId),
    defer: true,
    onSuccess: () => {
      getSprint.setData({ ...getSprint.data!, isComplete: true });
      notifySuccess("Sprint completed");
    },
  });
  return (
    <BoardContainer>
      <Header>
        {getSprint.inProgress ? (
          <Loading />
        ) : (
          <HeaderText>{getSprint.data?.name}</HeaderText>
        )}
        <ButtonContainer
          className={classname(getSprint.data?.isComplete && "completed")}
        >
          <CompleteSprintButton
            title="Complete sprint"
            className={classname(
              getSprint.data?.isComplete ? "completed" : "active"
            )}
            disabled={completeSprint.inProgress || getSprint.data?.isComplete}
            variant="outlined"
            startIcon={
              completeSprint.inProgress ? (
                <CircularProgress size={24} />
              ) : getSprint.data?.isComplete ? (
                <FaCheckCircle />
              ) : (
                <FiCheckCircle />
              )
            }
            onClick={completeSprint.execute}
          >
            {getSprint.data?.isComplete
              ? "Sprint completed"
              : "Complete sprint"}
          </CompleteSprintButton>
        </ButtonContainer>
      </Header>
      {getPosts.inProgress ? (
        <Loading />
      ) : (
        <Columns>
          <Column
            placeholder="What went well?"
            Icon={<FaRegCheckCircle size={20} style={{ color: "#65AEA0" }} />}
            onAdd={(content: string) =>
              addPost.execute({ content, type: "Good" })
            }
            onDelete={(postId: string) => deletePost.execute(postId)}
            posts={getPosts.data?.filter((post) => post.type === "Good")}
            canEdit={!getSprint.data?.isComplete}
          />
          <Column
            placeholder="Points of discussion"
            Icon={<FaRegLightbulb size={20} style={{ color: "#A38B00" }} />}
            onAdd={(content: string) =>
              addPost.execute({ content, type: "Improve" })
            }
            onDelete={(postId: string) => deletePost.execute(postId)}
            posts={getPosts.data?.filter((post) => post.type === "Improve")}
            canEdit={!getSprint.data?.isComplete}
          />
          <Column
            placeholder="What could have been done better?"
            Icon={<FaRegTimesCircle size={20} style={{ color: "#C89D9E" }} />}
            onAdd={(content: string) =>
              addPost.execute({ content, type: "Bad" })
            }
            onDelete={(postId: string) => deletePost.execute(postId)}
            posts={getPosts.data?.filter((post) => post.type === "Bad")}
            canEdit={!getSprint.data?.isComplete}
          />
        </Columns>
      )}
    </BoardContainer>
  );
};

export default Board;

const BoardContainer = styled.div`
  margin: 50px 50px 80px;
`;

const Columns = styled.div`
  display: flex;
  margin-top: 30px;
  @media screen and (1060px) {
    margin-top: 10px;
    flex-direction: column;
    > * {
      margin-bottom: 20px;
    }
  }
`;

const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  margin: 0.25em;

  &.completed {
    &:hover {
      cursor: not-allowed;
    }
  }
`;

const CompleteSprintButton = styled(Button)`
  border-color: #65aea0;

  &.active {
    color: #65aea0;
    background: white;
    border-color: #65aea0;
  }

  &.completed {
    color: #65aea0;
    background: white;
    border-color: #65aea0;
  }
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 0 2rem 0;
`;

const HeaderText = styled.h1`
  font-weight: 100;
  font-size: 2em;
  @media screen and (max-width: 500px) {
    font-size: 2em;
  }
`;
