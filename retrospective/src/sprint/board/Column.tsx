import React, { useState, useCallback } from "react";
import styled from "styled-components";
import { Post } from "../../api/types/Post";
import PostItem from "../../post/PostItem";
import Input from "./Input";
import { InputAdornment, TextField, colors } from "@material-ui/core";
import { BiSubdirectoryLeft } from "react-icons/bi";
import { ReactElement } from "react";

interface ColumnProps {
  posts?: Post[];
  placeholder: string;
  Icon: ReactElement;
  canEdit: boolean;
  onAdd: (content: string) => void;
  onDelete: (postId: string) => void;
}

const Column = ({
  posts,
  Icon,
  placeholder,
  canEdit,
  onAdd,
  onDelete,
}: ColumnProps) => {
  const [content, setContent] = useState("");
  const onContentChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => setContent(e.target.value),
    [setContent]
  );
  const onKeyDown = useCallback(
    (e: React.KeyboardEvent) => {
      if (e.keyCode === 13 && content) {
        onAdd(content);
        setContent("");
      }
    },
    [setContent, content]
  );
  return (
    <ColumnContainer>
      {canEdit && (
        <Add>
          <TextField
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">{Icon}</InputAdornment>
              ),
              endAdornment: (
                <EnterIcon>
                  <BiSubdirectoryLeft
                    size={32}
                    style={{ color: colors.grey[300] }}
                  />
                </EnterIcon>
              ),
            }}
            value={content}
            onChange={onContentChange}
            onKeyDown={onKeyDown}
            style={{ margin: 5 }}
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            fullWidth
            placeholder={placeholder}
          />
        </Add>
      )}
      <PostsContainer>
        {posts &&
          posts.map((post, index: number) => (
            <PostItem key={index} post={post} onDelete={onDelete} />
          ))}
      </PostsContainer>
    </ColumnContainer>
  );
};

export default Column;

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  margin-bottom: 10px;
  padding: 0 5px;
`;

const Add = styled.div`
  align-items: center;
  margin-bottom: 20px;
  display: flex;
  > :first-child {
    flex: 1;
  }
`;

const PostsContainer = styled.div`
  flex: 1;
  min-height: 100px;
`;

// const PostsWrapper = styled.div<{
//   draggingOver: boolean;
//   draggingColor: string;
// }>`
//   background-color: ${(props) =>
//     props.draggingOver ? props.draggingColor : 'unset'};
//   flex: 1;
//   min-height: 100px;
// `;

const EnterIcon = styled.div`
  display: flex;
  align-items: center;
  @media (max-width: 600px) {
    display: none;
    visibility: hidden;
  }
`;
