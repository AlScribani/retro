import { Sprint } from "./Sprint";

export type TeamsList = {
  name: string;
  code: string;
  colour: string;
};

export type Team = {
  id: string;
  name: string;
  code: string;
  colour: string;
  sprints: Sprint[];
};
// combine FUNCTION types and these types :D
