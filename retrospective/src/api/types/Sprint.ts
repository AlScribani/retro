import { User } from "./User";

export type Sprint = {
  id: string;
  name: string;
  teamId: string;
  isComplete: boolean;
  createdBy: string;
  lastUpdatedAt: Date;
};

export type ActiveSprint = {
  id: string;
  name: string;
  teamId: string;
  isComplete: boolean;
  createdBy: string;
  lastUpdatedAt: Date;
  postCount?: number;
  likesCount?: number;
  actionsCount?: number;
  participants?: User[];
};
