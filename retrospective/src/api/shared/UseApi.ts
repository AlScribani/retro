import { useState, useEffect } from "react";
import { AxiosError, AxiosResponse } from "axios";

type Task = {
  action: (...args: any[]) => Promise<AxiosResponse>;
  defer?: boolean;
  condition?: boolean;
  onSuccess?: Function;
  onError?: Function;
};

type UseApiResponse<T> = {
  inProgress: boolean;
  error: AxiosError | null;
  data: T | null;
  setData: React.Dispatch<React.SetStateAction<T | null>>;
  execute: (...args: any[]) => Promise<void>;
};

const useApi = <T>(task: Task, deps?: any): UseApiResponse<T> => {
  const [data, setData] = useState<T | null>(null);
  const [inProgress, setInProgress] = useState(false);
  const [error, setError] = useState<AxiosError | null>(null);

  const execute = async (params = null) => {
    try {
      setInProgress(true);
      const result = await task.action(params);
      console.log("result", result);
      setData(result.data);
      setInProgress(false);

      task.onSuccess && task.onSuccess(result);
    } catch (e) {
      console.log("e", e);
      setError(e);
      setInProgress(false);

      task.onError && task.onError(e);
    }
  };

  useEffect(() => {
    if (task.defer || task.condition === false) return;

    execute();
  }, deps || []);

  return {
    data,
    inProgress,
    error,
    setData,
    execute,
  };
};

export default useApi;
