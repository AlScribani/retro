import axios from "axios";

const BASE_URL = "http://localhost:5001/retro-board-8f06b/us-central1/api"

const instance = axios.create({
  baseURL: BASE_URL,
});

export default instance;
