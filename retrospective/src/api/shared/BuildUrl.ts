const buildUrl = (controllerName: string, endPoint?: string) =>
  `/${controllerName}/${endPoint || ""}`;

export default buildUrl;
