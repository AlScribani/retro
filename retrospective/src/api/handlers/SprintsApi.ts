import axios from "../shared/Axios";
import buildUrl from "../shared/BuildUrl";

const SPRINTS_CONTROLLER = "sprint";

export const getSprintsByTeam = async (teamId: string) =>
  axios.get(buildUrl(SPRINTS_CONTROLLER, teamId));

export const getActiveSprints = async (teamId: string) =>
  axios.get(buildUrl(SPRINTS_CONTROLLER, `${teamId}/active-sprints`));

export const getSprint = async (teamId: string, sprintId: string) =>
  axios.get(buildUrl(SPRINTS_CONTROLLER, `${teamId}/${sprintId}`)); //TODO: Need both team and sprint id as will eventually match on team and sprint name

export const createSprint = async (teamId: string, userId: string) =>
  axios.post(buildUrl(SPRINTS_CONTROLLER), { teamId, userId });

export const completeSprint = async (sprintId: string) =>
  axios.post(buildUrl(SPRINTS_CONTROLLER, "complete"), { sprintId });

export const reactivateSprint = async (sprintId: string) =>
  axios.post(buildUrl(SPRINTS_CONTROLLER, "reactivate"), { sprintId });

export const deleteSprint = async (sprintId: string) =>
  axios.delete(buildUrl(SPRINTS_CONTROLLER, sprintId)); //TODO: Sprint names will clash if new sprint created after another one is deleted
