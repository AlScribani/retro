import { useCallback } from "react";
import axios from "../shared/Axios";
import buildUrl from "../shared/BuildUrl";

const USERS_CONTROLLER = "user";

export const guestLogin = async (
  name: string,
  onUser: (user: string) => void
) => {
  if (name.length) {
    onUser(name);
    setNameToLocalStorage(name);
  }
};

const setNameToLocalStorage = (name: string) =>
  localStorage.setItem("name", name);

export const login = async (email: string, password: string) =>
  axios.post(buildUrl(USERS_CONTROLLER, "login"), { email, password });

export const signup = async (
  name: string,
  email: string,
  password: string,
  colour: string
) =>
  axios.post(buildUrl(USERS_CONTROLLER, "signup"), {
    name,
    email,
    password,
    colour,
  });
