import axios from "../shared/Axios";
import buildUrl from "../shared/BuildUrl";

const POSTS_CONTROLLER = "post";

export const getPosts = async (sprintId: string) =>
  axios.get(buildUrl(POSTS_CONTROLLER, sprintId));

export const addPost = async (
  sprintId: string,
  content: string,
  type: string,
  userId: string
) =>
  axios.post(buildUrl(POSTS_CONTROLLER), {
    content,
    sprintId,
    type,
    userId,
  });

export const likePost = async (postId: string, userId: string) =>
  axios.post(buildUrl(POSTS_CONTROLLER, `like/${postId}`), { userId });

export const unlikePost = async (postId: string, userId: string) =>
  axios.post(buildUrl(POSTS_CONTROLLER, `unlike/${postId}`), { userId });

export const deletePost = async (postId: string) =>
  axios.delete(buildUrl(POSTS_CONTROLLER, postId));
