import axios from "../shared/Axios";
import buildUrl from "../shared/BuildUrl";

const TEAMS_CONTROLLER = "team";

export const getTeams = async () => axios.get(buildUrl(TEAMS_CONTROLLER));

export const getTeam = async (teamId: string) =>
  axios.get(buildUrl(TEAMS_CONTROLLER, teamId));
